FROM openjdk:13-alpine
VOLUME /tmp
ADD /target/*.jar open-weather-api-0.0.1-SNAPSHOT.jar
COPY /target/*.jar open-weather-api-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/open-weather-api-0.0.1-SNAPSHOT.jar"]