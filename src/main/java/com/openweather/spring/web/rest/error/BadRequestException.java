package com.openweather.spring.web.rest.error;

import org.zalando.problem.Status;

/**
 * Bad request exception
 */
public class BadRequestException extends CustomException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public BadRequestException(String title, String detail) {
        super(title, Status.NOT_FOUND, detail);
    }

}
