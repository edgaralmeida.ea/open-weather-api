package com.openweather.spring.web.rest.error;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Problem;
import org.zalando.problem.StatusType;

/**
 * Custom exception
 */
public class CustomException extends AbstractThrowableProblem { 

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    protected CustomException(String title, StatusType status, String detail) {
        super(Problem.DEFAULT_TYPE, title, status, detail);      
    }
 

}
