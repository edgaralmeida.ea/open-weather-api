package com.openweather.spring.web.rest.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.openweather.spring.api.WeatherResponse;
import com.openweather.spring.constants.HttpHeaderConstants;
import com.openweather.spring.domain.WeatherEntity;
import com.openweather.spring.service.WeatherService;
import com.openweather.spring.utils.ResponseUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.parsing.Problem;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;


@RequestMapping(value = "/api")
@RestController
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = WeatherEntity.class))),
            @ApiResponse(responseCode = "400", content = @Content(mediaType = MediaType.APPLICATION_PROBLEM_JSON_VALUE, schema = @Schema(implementation = Problem.class))),
            @ApiResponse(responseCode = "404", content = @Content(mediaType = MediaType.APPLICATION_PROBLEM_JSON_VALUE, schema = @Schema(implementation = Problem.class))),
            @ApiResponse(responseCode = "500", content = @Content(mediaType = MediaType.APPLICATION_PROBLEM_JSON_VALUE, schema = @Schema(implementation = Problem.class))) })
    @GetMapping(value = "/weather/{city}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<WeatherEntity> weather(HttpServletRequest request,@RequestHeader(value = HttpHeaderConstants.REQUEST_ID, required = true) @NotBlank String traceid,
    @RequestHeader(value = HttpHeaderConstants.ENVIRONMENT, required = true) @NotBlank String environment,
            @PathVariable(required = true) @NotNull String city) {

        ResponseEntity<WeatherResponse> response = weatherService.getWeather(city);

        Optional<WeatherEntity> weatherEntity = weatherService.createOrUpdateWeather(response.getBody());

        return ResponseUtils.okOrNotFound(MediaType.APPLICATION_JSON, weatherEntity);
    }

}
