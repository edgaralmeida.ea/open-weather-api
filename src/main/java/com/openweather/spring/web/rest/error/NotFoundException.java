package com.openweather.spring.web.rest.error;

import org.zalando.problem.Status;

/**
 * Not found request exception
 */
public class NotFoundException extends CustomException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NotFoundException(String title, String detail) {
        super(title, Status.NOT_FOUND, detail);
    }

}
