package com.openweather.spring.aop;

import javax.servlet.http.HttpServletRequest;

import com.openweather.spring.constants.HttpHeaderConstants;
import com.openweather.spring.web.rest.error.BadRequestException;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


/**
 * Aspect that validates headers values
 */
@Aspect
@Configuration
public class HttpHeaderAspect {

    private final Environment environment;
   

    public HttpHeaderAspect(Environment environment) {
        this.environment = environment;
    }

    
    /**
     * Pointcut that matches all REST endpoints
     */
    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void restController() {
        // Method is empty once this is just a pointcut
    }

 
    /**
     * Called on RestController  pointcut
     */
    @Before("within(java.com.openweather.spring.web.rest..*) && within(@org.springframework.web.bind.annotation.RestController *)")
    public void validateHeaders() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        
        if (!validateEnvironment(request.getHeader(HttpHeaderConstants.ENVIRONMENT))) {
            throw new BadRequestException("Invalid Environment", String.format("Environment '%s' is not valid.", request.getHeader(HttpHeaderConstants.ENVIRONMENT)));
        }
    }

    /**
     * Validate current active Environment
     */
    private boolean validateEnvironment(String environment) {
        for (String actProfile : this.environment.getActiveProfiles()) {
            if (actProfile.equalsIgnoreCase(environment)) {
                return true;
            }
        }

        return false;
    }

}
