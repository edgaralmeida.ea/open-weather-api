package com.openweather.spring.mapper;

import com.openweather.spring.api.WeatherResponse;
import com.openweather.spring.domain.WeatherEntity;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;


@Mapper(componentModel = "spring", uses = { WeatherMapper.class } )
public interface WeatherMapper{

    default WeatherEntity toEntity(WeatherResponse weatherResponse) {
   
        if (weatherResponse == null) {
            return null;
        }

        WeatherEntity weatherEntity = new WeatherEntity();
        weatherEntity.setId(weatherResponse.getId());
        weatherEntity.setCity(weatherResponse.getName());
        weatherEntity.setCountry(weatherResponse.getSys() != null ? weatherResponse.getSys().getCountry():null);
        weatherEntity.setTemperature(weatherResponse.getMain() != null ? weatherResponse.getMain().getTemp():null);

        return weatherEntity;

    }

    default WeatherEntity updateWeatherEntity(WeatherResponse weatherResponse, @MappingTarget WeatherEntity weatherEntity) {

        if (weatherResponse == null || weatherEntity == null) {
            return null;
        }

        weatherEntity.setCity(weatherResponse.getName());
        weatherEntity.setCountry(weatherResponse.getSys() != null ? weatherResponse.getSys().getCountry():null);
        weatherEntity.setTemperature(weatherResponse.getMain() != null ? weatherResponse.getMain().getTemp():null);

        return weatherEntity;

    }


    
}
