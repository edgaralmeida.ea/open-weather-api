package com.openweather.spring.service;

import java.util.Optional;

import com.openweather.spring.api.WeatherResponse;
import com.openweather.spring.constants.Constants;
import com.openweather.spring.domain.WeatherEntity;
import com.openweather.spring.mapper.WeatherMapper;
import com.openweather.spring.repository.WeatherRepository;
import com.openweather.spring.web.rest.error.NotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import org.springframework.web.client.RestTemplate;

@Service
@Transactional
public class WeatherService {

    private final Logger log = LoggerFactory.getLogger(WeatherService.class);

    @Autowired
    private WeatherRepository weatherRepository;

    @Autowired
    private WeatherMapper weatherMapper;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * Create or udpdate weather entity.
     *
     * @param WeatherResponse
     * @return WeatherEntity with details
     */
    public Optional<WeatherEntity> createOrUpdateWeather(WeatherResponse weatherResponse) {
        log.debug("createOrUpdateWeather(id: {})", weatherResponse.getId());

        Optional<WeatherEntity> weatherDB = weatherRepository.findById(weatherResponse.getId());
        if (weatherDB.isPresent()) {

            return Optional
                    .of(weatherRepository.save(weatherMapper.updateWeatherEntity(weatherResponse, weatherDB.get())));
        } else {

            return Optional.of(weatherRepository.save(weatherMapper.toEntity(weatherResponse)));

        }
    }

    /**
     * Create or udpdate weather entity.
     *
     * @param City
     * @return ResponseEntity<WeatherResponse> with details
     * 
     *         This method implements a cache mechanism and store it on "weather".
     */
    @Cacheable(value = "weather")
    public ResponseEntity<WeatherResponse> getWeather(String city) {

        log.debug("getWeather(city: {})", city);
        
        String url = Constants.WEATHER_API_URL.replace("{city}", city).replace("{appid}", Constants.APP_ID);

        ResponseEntity<WeatherResponse> result;

        try {
            
           result = restTemplate.getForEntity(url, WeatherResponse.class);

        } catch (NotFound e) {
            throw new NotFoundException("Invalid city", String.format("City '%s' is not valid.", city));
        }

        return result;
    }

}
