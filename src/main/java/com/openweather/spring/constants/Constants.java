package com.openweather.spring.constants;

public class Constants {

    public static final String APP_ID = "6802d09e60113425fee0783301327970";

    public static final String WEATHER_API_URL = "http://api.openweathermap.org/data/2.5/weather?q={city}&APPID={appid}";
}
