package com.openweather.spring.constants;

public class HttpHeaderConstants {

    //to identify request Id
    public static final String REQUEST_ID = "request-id";
    
    //to identify environment
    public static final String ENVIRONMENT = "environment";

    
}
