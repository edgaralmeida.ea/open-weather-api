package com.openweather.spring.utils;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public class ResponseUtils {

    
    public static <T> ResponseEntity<T> okOrError(MediaType contentType, Optional<T> maybeResponse) {

        return maybeResponse.map(response -> ResponseEntity.ok().contentType(contentType).body(response))
                .orElse(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(contentType).build());
    }

    public static <T> ResponseEntity<T> okOrNotFound(MediaType contentType, Optional<T> maybeResponse) {
        return maybeResponse.map(response -> ResponseEntity.ok().contentType(contentType).body(response))
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(contentType).build());
    }
    
}
