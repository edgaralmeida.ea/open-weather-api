package com.openweather.spring;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openweather.spring.api.WeatherResponse;
import com.openweather.spring.constants.HttpHeaderConstants;
import com.openweather.spring.domain.WeatherEntity;
import com.openweather.spring.mapper.WeatherMapper;
import com.openweather.spring.service.WeatherService;
import com.openweather.spring.web.rest.controller.WeatherController;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;
import org.springframework.web.util.NestedServletException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

	private MockMvc restMockMvc;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private Validator validator;

	@Autowired
	private WeatherController wheaterController;

	@Autowired
	private WeatherMapper weatherMapper;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WeatherService weatherService;

	private WeatherEntity wheatherEntity;

	private String REQUEST_ID = "test";

	private String ENVIRONMENT = "dev";

	@Before
	public void setup() {

		this.restMockMvc = MockMvcBuilders.standaloneSetup(wheaterController)
				.setCustomArgumentResolvers(pageableArgumentResolver).setMessageConverters(jacksonMessageConverter)
				.setValidator(validator).build();

		this.wheatherEntity = new WeatherEntity();
		this.wheatherEntity.setCity("Leiria");

	}

	@Test
	public void contextLoads() {
		assertTrue(true);
	}

	@Test
	public void callWeatherWithoutRequestId() throws Exception {

		restMockMvc.perform(get("/api/weather/{city}", wheatherEntity.getCity())
				.header(HttpHeaderConstants.ENVIRONMENT, ENVIRONMENT).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isBadRequest());

	}

	@Test
	public void callWeatherWithoutEnvironment() throws Exception {

		restMockMvc.perform(get("/api/weather/{city}", wheatherEntity.getCity())
				.header(HttpHeaderConstants.REQUEST_ID, REQUEST_ID).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isBadRequest());

	}

	@Test
	public void callWeatherApiwithEmptyParameter() throws Exception {

		restMockMvc.perform(get("/api/weather/{city}", "").header(HttpHeaderConstants.REQUEST_ID, REQUEST_ID)
				.header(HttpHeaderConstants.ENVIRONMENT, ENVIRONMENT).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isNotFound());

	}

	@Test
	public void callWeatherApiwithInvalidParameter() {

		try {
			restMockMvc
					.perform(get("/api/weather/{city}", "123456789").header(HttpHeaderConstants.REQUEST_ID, REQUEST_ID)
							.header(HttpHeaderConstants.ENVIRONMENT, ENVIRONMENT)
							.contentType(MediaType.APPLICATION_JSON_VALUE))
					.andExpect(result -> assertTrue(result.getResolvedException() instanceof NestedServletException));
		} catch (Exception e) {
		}

	}

	@Test
	public void callWeatherApiwithSucess() throws Exception {

		restMockMvc.perform(get("/api/weather/{city}", wheatherEntity.getCity())
				.header(HttpHeaderConstants.REQUEST_ID, REQUEST_ID).header(HttpHeaderConstants.ENVIRONMENT, ENVIRONMENT)
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());

	}

	@Test
	public void callWeatherApiwithSucessAndValidate() throws Exception {

		MvcResult result = restMockMvc.perform(get("/api/weather/{city}", wheatherEntity.getCity())
				.header(HttpHeaderConstants.REQUEST_ID, REQUEST_ID).header(HttpHeaderConstants.ENVIRONMENT, ENVIRONMENT)
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk()).andReturn();

		WeatherEntity reponse = objectMapper.readValue(result.getResponse().getContentAsString(), WeatherEntity.class);

		assertNotNull(reponse.getCity());
		assertNotNull(reponse.getId());
		assertNotNull(reponse.getCountry());
		assertNotNull(reponse.getTemperature());

	}

	@Test
	public void callUpdateWeatherEntityServiceNull() throws Exception {

		WeatherEntity reponse = weatherMapper.updateWeatherEntity(null, null);

		assertNull(reponse);
	}

	@Test
	public void callGetWeatherServiceMapToEntityNull() throws Exception {

		WeatherEntity reponse = weatherMapper.toEntity(null);

		assertNull(reponse);
	}

	@Test
	public void callUpdateWeatherEntityService() throws Exception {

		WeatherEntity reponse = weatherMapper.updateWeatherEntity(new WeatherResponse(), wheatherEntity);

		assertNotNull(reponse);
		assertNull(reponse.getCity());
		assertNull(reponse.getId());
		assertNull(reponse.getCountry());
		assertNull(reponse.getTemperature());
	}

	@Test
	public void callGetWeatherServiceMapToEntity() throws Exception {

		WeatherEntity reponse = weatherMapper.toEntity(new WeatherResponse());

		assertNotNull(reponse);
		assertNull(reponse.getCity());
		assertNull(reponse.getId());
		assertNull(reponse.getCountry());
		assertNull(reponse.getTemperature());
	}

	@Test
	public void callGetWeatherServiceMapToEntityNotNull() throws Exception {

		ResponseEntity<WeatherResponse> result = weatherService.getWeather(wheatherEntity.getCity());

		WeatherEntity reponse = weatherMapper.toEntity(result.getBody());

		assertEquals(result.getBody().getClass(), WeatherResponse.class);
		assertEquals(reponse.getClass(), WeatherEntity.class);
		assertNotNull(reponse.getCity());
		assertNotNull(reponse.getId());
		assertNotNull(reponse.getCountry());
		assertNotNull(reponse.getTemperature());

	}

}
