Open Weather API
---

### Introduction

This is a spring boot API that exposes only one endpoint, consumes data from [OpenWeather(https://openweathermap.org/appid] api and stores the result on a database.

### Git Clone
- https://gitlab.com/edgaralmeida.ea/open-weather-api.git

### Start the App

./mvnw spring-boot:run

Using docker:

- docker-compose up web 

server port 8010 defined on docker-compose.yml


### H2 Database
H2 in memory database, you can access to console ui at http://localhost:8010/h2 with the following JDBC URL -> jdbc:h2:mem:openweather
On a real production environment we should have a dedicated database (ex:PostgresSql) with defined jdbc url on application-env.yml 

### Database Schema

CREATE TABLE weather (
    id INT PRIMARY KEY,    
    city VARCHAR(255) NOT NULL,
    country VARCHAR(255) NOT NULL,
    temperature NUMERIC(5, 2)
);

Change id from Generation.Auto to the city id returned by the api to avoid duplicated results on DB.
On a final solution we can use both, our internal id and the city id returned by the api.


### Swagger Ui
http://localhost:8010/swagger-ui.html

### Actuators
Info -> http://localhost:8010/management/info
Health -> http://localhost:8010/management/health 
Metrics -> http://localhost:8010/management/metrics
Logs -> http://localhost:8010/management/loggers

### Endpoint definition
@Path - http://localhost:8010/api/weather/{city}

Required Parameters:

@HeaderParam String request-id 

@HeaderParam String environment

@PathParam String city -> required


Http Return status:

- 200 - OK 
Schema:
 {
  "id": 0,
  "city": "string",
  "country": "string",
  "temperature": 0
}

- 400 - Bad Request

- 404 - Custom exception for Not fount ()
{
  "title": "Invalid city",
  "status": 404,
  "detail": "City '1234436564' is not valid."
}

- 500 - Internal Server Error

### Service
@Service - > WeatherService

### Jpa Repository
@Repository -> WeatherRepository




